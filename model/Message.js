const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    name : {
        type: String,
        required: [true, 'Name is required']
    },
    email : {
        type: String,
        required: [true, 'Email is required']
    },
    mobileNo : {
        type: Number,
        required: [true, 'Number is required']
    },
    message : {
        type: String,
        required: [true, 'Message is required']
    },
})

module.exports = mongoose.model('Message', messageSchema);
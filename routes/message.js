const express = require('express');
const messageController = require('../controllers/message');

const router = express.Router();

router.post("/add", messageController.addMessage);

module.exports = router;
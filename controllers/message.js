const Message = require("../model/Message");

module.exports.addMessage = (req , res ) => {
    let newMessage = new Message({
        name: req.body.name,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        message: req.body.message
    })

    return newMessage.save().then((message, error) => {
        if(error){
            return res.send(false);
        } else {
            return res.send(true);
        }
    }).catch(err => res.send(err))
}